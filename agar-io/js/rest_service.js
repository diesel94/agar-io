const SERVER = "localhost";

const SERVICE_URL_PATH = "http://" + SERVER + ":8080/AgarClone/Server?operation=";
const SET_NAME = "setName";
const GET_MAP = "getMap";
const MOVE = "move"
var PLAYER_ID;

function setName()
{
	$.ajax(
	{
		type: "GET",
		dataType: "json",
		url: (SERVICE_URL_PATH + SET_NAME + "&param=" + sessionStorage.playerName)
	})
	.done(function(json)
	{
		var playerDic = [];
		$.each(json, function(key, value)
		{
			playerDic[key] = value;
		});
		//console.log(playerDic);
		player = new Player(playerDic["id"], 
			playerDic["name"], 
			playerDic["x"], playerDic["y"],
			playerDic["radius"]);
		PLAYER_ID = playerDic["id"];

		getMapRequest();
	})
	.fail(function()
	{
		//console.log(SET_NAME + " error");
	});
	startEngine();
}

function getMapRequest()
{
	$.ajax(
	{
		type: "GET",
		dataType: "json",
		url: (SERVICE_URL_PATH + GET_MAP)
	})
	.done(function(json)
		{
			var mapDic = {};
			var foodDics = new Array();
			var playerKilled = true;
			enemy = new Array();
			$.each(json, function(key, value)
			{
				mapDic[key] = value;
			});
			$.each(mapDic["players"], function(key, value)
			{
				if(value["id"] == PLAYER_ID)
				{
					//console.log("player: " + value);
					playerKilled = false;
					player.body.radius = value["radius"];
				}
				else
				{
					//console.log("enemy: " + value);
					enemy.push(new Enemy(value["id"], value["x"], value["y"], value["radius"], value["name"]));
				}
			});
			$.each(mapDic["food"], function(key, value)
			{
				var foodDic = {};
				$.each(value, function(key, value)
				{
					foodDic[key] = value;
				});
				foodDics.push(foodDic);
			});
			if(playerKilled)
			{
				gameOver(-1);
			}
			onGetMap(mapDic, foodDics);
		})
	.fail(function()
		{
			console.log(GET_MAP + " error");
		});
}

function moveRequest(x, y)
{
	x = -x;
	y = -y;
	$.ajax(
	{
		type: "GET",
		dataType: "json",
		url: (SERVICE_URL_PATH + MOVE + "&x=" + x + "&y=" + y)
	})
	.done(function(json)
	{
		$.each(json, function(key, value)
		{
			if(key == "event")
			{
				//console.log(key + ":" + value + " x=" + x + " y=" + y);
				if(value != "Invalid move")
				{
					player.body.x += x;
					player.body.y += y;
					viewport.setCenterPosition(-x, -y);
				}
				if(value == "Eat")
				{

				}
				else if(value == "Killed")
				{	
					gameOver(json["killer"]);
				}
			}
		});
	})
	.fail(function()
	{
		console.log(MOVE + " error");
	})
}

function onGetMap(mapDic, foodDics)
{
	viewport.setMap(mapDic["width"], mapDic["height"]);
	generateFood(foodDics);
	getFrame();
}

setName();
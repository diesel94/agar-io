var canvas  = document.getElementById("mainCanvas");
var context = canvas.getContext("2d");

function Viewport()
{
	this.x = 0;
	this.y = 0;
	this.width = 0;
	this.height = 0;
	
	this.setMap = function(width, height)
	{
		this.width = width;
		this.height = height;
		this.setCenterPosition(0, 0);
	}
	this.setCenterPosition = function(x, y)
	{
		this.translate(this.getCenterX() - player.body.x, this.getCenterY() - player.body.y);
	}
	this.getCenterX = function()
	{
		return ((canvas.width / 2) - this.x);
	}
	this.getCenterY = function()
	{
		return ((canvas.height / 2) - this.y);	
	}
	this.getOriginX = function()
	{
		return -this.x;
	}
	this.getOrignY = function()
	{
		return -this.y;
	}
	this.reset = function()
	{
		viewport.translate(-this.x, -this.y);
	}
	this.translate = function(x, y)
	{
		this.x += x;
		this.y += y;

		if(this.getCenterX() <= 0)
		{
			this.x = (canvas.width/2);
		}
		else if(this.getCenterX() >= this.width)
		{
			this.x = (canvas.width/2) - this.width;
		}
		else
		{
			context.translate(x, 0);
		}

		if(this.getCenterY() <= 0)
		{
			this.y = (canvas.height/2);
		}
		else if(this.getCenterY() >= this.height)
		{
			this.y = (canvas.height/2) - this.height;
		}
		else
		{
			context.translate(0, y);
		}

		//console.log("viewport center (" + this.getCenterX() + ", " + this.getCenterY() + ")");
	}
	this.clear = function()
	{
		context.fillStyle = MAP_CLEAR_COLOR;
		context.fillRect(-this.x, -this.y, this.width + (canvas.width/2), this.height + (canvas.height/2));
		context.fillStyle = MAP_BG_COLOR;
		context.fillRect(0, 0, this.width, this.height);
	}
}
var viewport = new Viewport();
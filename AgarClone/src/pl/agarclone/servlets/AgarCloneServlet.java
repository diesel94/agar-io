package pl.agarclone.servlets;
 
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.LinkedList;

import pl.agarclone.game.*;

public class AgarCloneServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final int mapWidth = 1000;
    private static final int mapHeigth = 1000;

    private List<GameMap> rooms;
    
    public void init()
    {
    	this.rooms = new LinkedList<GameMap>();
    }
    
    // returns first room that isn't full, or a new instance
    public GameMap getAvailableRoom()
    {
    	for(int i = 0; i < this.rooms.size(); i++)
    	{
    		if(!this.rooms.get(i).IsFull())
    		{
    			return this.rooms.get(i);
    		}
    	}
    	GameMap newRoom = new GameMap(mapWidth, mapHeigth);
    	this.rooms.add(newRoom);
    	return newRoom;
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
    	response.addHeader("Content-type", "application/json;charset=UTF-8");
    	PrintWriter out = response.getWriter();
    	String operation = request.getParameter("operation");   	
    	
    	HttpSession session = request.getSession();
    	String sessionID = session.getId();
    	Player player = (Player)session.getAttribute("Player");
    	GameMap room = (GameMap)session.getAttribute("Room");
    	
    	if(operation == null)
    		return;
    	
    	// if player is in an existing room
    	if(room == null)
    	{
    		room = getAvailableRoom();
    		session.setAttribute("Room", room);
    	}
    	// if player doesn't exist, create him
    	if(player == null || !room.PlayerExists(sessionID))
    	{
    		player = room.SpawnNewPlayer(sessionID);
    		session.setAttribute("Player", player);
    	}
    	
    	// ============================ GET PARAMETERS ===========================
    	// send client current map state
    	if(operation.equals("getMap"))
    	{
    		out.println(room.getMap());
    	}
    	// set player's name
    	else if(operation.equals("setName"))
    	{
    		String param = request.getParameter("param");
    		if(param == null)
    			return;
    		player.setName(param);
    		out.print(player.getJson().toString());
    	}
    	// move player
    	else if(operation.equals("move"))
    	{
    		if(request.getParameter("x") == null || request.getParameter("y") == null)
    			return;
    		
    		float valueX = Float.parseFloat(request.getParameter("x"));
    		float valueY = Float.parseFloat(request.getParameter("y"));
    		out.print(room.MovePlayer(player, valueX, valueY));
    	}
    	
    }
}
package pl.agarclone.game;

import javax.json.Json;
import javax.json.JsonObject;

public class Food extends GameObject {
	public Food(int x, int y, float radius)
	{
		super(x, y, radius);
	}
	
	public JsonObject getJson()
	{
		return Json.createObjectBuilder()
				.add("id", this.id)
				.add("x", this.x)
				.add("y", this.y)
				.add("radius", this.radius)
				.build();
	}
}

package pl.agarclone.game;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

public class GameMap {
	private static final int maxPlayers = 5; // max Players per room
	private static final int maxFood = 20;
	private Random generator;
	
	private int width;
	private int height;
	private HashMap<String, Player> players;
	private List<Food> food;
	
	public GameMap(int width, int height)
	{
		this.width = width;
		this.height = height;
		this.players = new HashMap<String, Player>();
		this.food = new LinkedList<Food>();
		generator = new Random();
		
		for(int i = 0; i < GameMap.maxFood; i++)
		{
			SpawnFood();
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	// randomize position of GameObject
	public void RandomizePosition(GameObject go)
	{
		int randomX = generator.nextInt(this.width);
		int randomY = generator.nextInt(this.height);
		go.setX(randomX);
		go.setY(randomY);
	}
	
	// spawn new player
	public Player SpawnNewPlayer(String sessionID)
	{
		Player newPlayer = new Player(0, 0, 10, sessionID, this);
		RandomizePosition(newPlayer);
		this.players.put(sessionID, newPlayer);
		
		return newPlayer;
	}
	
	// spawn food
	public Food SpawnFood()
	{
		if(this.food.size() > GameMap.maxFood)
			return null;
		
		Food f = new Food(0, 0, 10);
		RandomizePosition(f);
		this.food.add(f);
		
		return f;
	}
	
	// convert food list into json
	public JsonArray getFoodJson()
	{
		JsonArrayBuilder foodJson = Json.createArrayBuilder();
		for(Food f : food)
		{
			foodJson.add(f.getJson());
		}
		return foodJson.build();
	}
	
	// convert players hashmap into json
	public JsonArray getPlayersJson()
	{
		JsonArrayBuilder playersJson = Json.createArrayBuilder();
		for(Entry<String, Player> entry : players.entrySet()) {
		    Player p = entry.getValue();
		    playersJson.add(p.getJson());
		}
		return playersJson.build();
	}
	
	// generate string json values for entire map
	public String getMap()
	{
		return Json.createObjectBuilder()
			.add("width", this.width)
			.add("height", this.height)
			.add("players", getPlayersJson())
			.add("food", getFoodJson())
			.build().toString();
	}
	
	// checks if player have eaten something, returns Json array with eaten objects
	// if we get inside someone bigger - returns null
	public JsonArray CheckEaten(Player player)
	{
		List<String> eatenPlayers = new ArrayList<String>();
		List<Food> eatenFood = new ArrayList<Food>();
		JsonArrayBuilder eatenJson = Json.createArrayBuilder();
		
		// collisions with other players
		for(Entry<String, Player> entry : players.entrySet()) {
		    Player other = entry.getValue();
		    
		    // ourself
		    if(other.id == player.id)
		    	continue;
		    
		    // tie
		    if(other.radius == player.radius)
		    {
		    	continue;
		    }
		    
		    // someone is eating us, 'cause he's bigger
		    if(other.Contains(player) && other.radius > player.radius)
		    {
		    	other.Eat(player);
		    	player.setKilledBy(other.id);
		    	return null;
		    }
		    
		    // we are eating someone
		    if(player.Contains(other))
		    {
		    	player.Eat(other);
		    	eatenPlayers.add(other.getSessionID());
		    	eatenJson.add(other.id);
		    }

		}
		
		// lets check if any food is within our radius
		for(Food f : this.food)
		{
			if(player.Contains(f))
			{
				player.Eat(f);
				eatenFood.add(f);
				eatenJson.add(f.id);
			}
		}
		
		// clear up food and players list
		for(String sessionID : eatenPlayers)
		{
			players.remove(sessionID);
		}
		for(Food f : eatenFood)
		{
			food.remove(f);
			SpawnFood();
		}
		
		return eatenJson.build();
	}
	
	// moves player and checks if he eaten something. Returns string JSON.
	public String MovePlayer(Player player, float valueX, float valueY)
	{
		JsonObjectBuilder reply = Json.createObjectBuilder();
		
		if(!player.Move(valueX, valueY))
		{
			// invalid move, player escapes map
			reply.add("event", "Invalid move");
		}
		else
		{
			// valid move, player stays on map
			JsonArray eatList = CheckEaten(player);
			
			// we are dead
			if(eatList == null)
			{
				reply.add("event", "Killed");
				reply.add("killer", player.getKilledBy());
				players.remove(player.getSessionID());
			}
			// valid move, but nothing happened
			else if(eatList.size() == 0)
			{
				reply.add("event", "Valid move");
			}
			else
			{
				reply.add("event", "Eat");
				reply.add("eatlist", eatList);
			}
		}
		
		return reply.build().toString();
	}
	
	// check if player is still alive
	public boolean PlayerExists(String sessionID)
	{
		return players.containsKey(sessionID);
	}
	
	// check if this instance is full of players
	public boolean IsFull()
	{
		return this.players.size() >= GameMap.maxPlayers;
	}
}

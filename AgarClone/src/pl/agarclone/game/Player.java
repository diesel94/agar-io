package pl.agarclone.game;

import java.util.Random;

import javax.json.Json;
import javax.json.JsonObject;

public class Player extends GameObject {
	private Random generator;
	private String sessionID;
	private String name;
	private GameMap gameMap;
	private int killedBy; // id of last killer
	
	public Player(int x, int y, float radius, String sessionID, GameMap gameMap)
	{
		super(x, y, radius);
		this.sessionID = sessionID;
		this.gameMap = gameMap;
		this.generator = new Random();
		this.name = "";
		this.killedBy = -1;
	}
	
	public int getKilledBy() {
		return killedBy;
	}

	public void setKilledBy(int killedBy) {
		this.killedBy = killedBy;
	}

	public String getSessionID()
	{
		return this.sessionID;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	// Radius calculation after eating something
	public void Eat(GameObject other)
	{
		this.radius += other.getRadius()/10;
	}
	
	// move player
	public boolean Move(float valueX, float valueY)
	{
		float newX = this.x + valueX;
		float newY = this.y + valueY;
		
		if(newX >= 0 && newX <= this.gameMap.getWidth())
		{
			this.x = newX;
		}
		else
		{
			return false;
		}
		if(newY >= 0 && newY <= this.gameMap.getHeight())
		{
			this.y = newY;
		}
		else
		{
			return false;
		}
		return true;
	}
	
	public void RandomizePosition(int maxX, int maxY)
	{
		this.x = this.generator.nextInt(maxX);
		this.y = this.generator.nextInt(maxY);
	}
	
	public JsonObject getJson()
	{
		return Json.createObjectBuilder()
				.add("id", this.id)
				.add("x", this.x)
				.add("y", this.y)
				.add("radius", this.radius)
				.add("name", this.name)
				.build();
	}
	
	// check if given object is in players radius
	public boolean Contains(GameObject go)
	{
		float distance = (float)java.lang.Math.sqrt(java.lang.Math.pow(this.x-go.x, 2) + java.lang.Math.pow(this.y-go.y, 2));
		
		return distance < this.radius;
	}
	
}
